# -*- coding: utf-8 -*-
from utilities import keepdict
from utilities import pretty
class Config(keepdict):
	def __init__(self):
		super(Config, self).__init__('config/AllConfig.json')
	def Configure(self, DataConfig):
		Values = DataConfig.get('values')
		Keys = []
		for key, value in Values.items():
			Keys.append(int(key))
		Keys.sort()
		SubKey = None
		newData = keepdict('config/AllConfig.json')
		newData.empty()
		for i in range(len(Keys)):
			Value = Values.get(str(Keys[i]))
			newKey = str(Value.get('ConfigNome')).encode('utf-8')
			if newKey[0] == '_':
				SubKey = Value.get('ConfigValor').encode('utf-8')
				newData[SubKey] = {}
			else:
				if newKey in newData[SubKey]:
					if type(newData[SubKey][newKey]) != list:
						newData[SubKey][newKey] = [newData[SubKey][newKey]]

					newData[SubKey][newKey].append(Value.get('ConfigValor').encode('utf-8'))					
				else:
					newData[SubKey][newKey] = Value.get('ConfigValor').encode('utf-8')
		newData.save()