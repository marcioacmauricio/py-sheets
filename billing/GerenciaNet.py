# -*- coding: utf-8 -*-
from utilities import keepdict
from gerencianet import Gerencianet
import pdfkit
import jinja2
from mailer import Mailer
class GerenciaNet:
	Config = keepdict('config/Ticket.json')
	GN = None
	Status = True
	ErrorMessage = []
	TicketsData = keepdict('billing/Data.json')
	def getError(self):
		return '\n'.join(self.ErrorMessage)
	def addPayment(self,ProductName = '', ProductValue = '',ExpireAt = '', ClinetName = '', ClientEmail = '',CPF = '', BirthDay = '',PhoneNumber = '',Amount = 1,ShippingName = 'No shipping',ShippingValue = 0):
		if not bool(ProductName):
			self.Status = False
			self.ErrorMessage.append("'ProductName' not defined!")
		if not bool(ProductValue):
			self.Status = False
			self.ErrorMessage.append("'ProductValue' not defined!")
		if not bool(ExpireAt):
			self.Status = False
			self.ErrorMessage.append("'ExpireAt' not defined!")
		if not bool(ClinetName):
			self.Status = False
			self.ErrorMessage.append("'ClinetName' not defined!")
		if not bool(ClientEmail):
			self.Status = False
			self.ErrorMessage.append("'ClientEmail' not defined!")
		if not bool(CPF):
			self.Status = False
			self.ErrorMessage.append("'CPF' not defined!")
		if not bool(BirthDay):
			self.Status = False
			self.ErrorMessage.append("'BirthDay' not defined!")
		if not bool(PhoneNumber):
			self.Status = False
			self.ErrorMessage.append("'PhoneNumber' not defined!")
		if bool(self.Status):
			# print(self.TicketsData)
			NewTicket = {
				'config': self.Config,
				'items': [{
					'name': ProductName,
					'value': ProductValue,
					'amount': Amount
				}],
				'shippings': [{
					'name': ShippingName,
					'value': ShippingValue
				}],
				'payment': {
					'banking_billet': {
						'expire_at': '2017-10-04',
						'customer': {
							'name': "Gorbadoc Oldbuck",
							'email': "oldbuck@gerencianet.com.br",
							'cpf': "94271564656",
							'birth': "1977-01-15",
							'phone_number': "5144916523"
						}
					}
				},
				'params': {
					'ProductName':ProductName,
					'ProductValue':ProductValue,
					'ExpireAt': ExpireAt,
					'ClinetName':ClinetName,
					'ClientEmail':ClientEmail,
					'CPF':CPF,
					'BirthDay':BirthDay,
					'PhoneNumber':PhoneNumber,
					'Amount':Amount,
					'ShippingName':ShippingName,
					'ShippingValue':ShippingValue
				}
			}
			self.TicketsData.update({len(self.TicketsData):NewTicket})
			self.TicketsData.save()
		return self.Status
	def ProcessPayment(self):
		Tickets = keepdict('billing/Issued.json')
		options = {
			'client_id': self.Config['client_id'],
			'client_secret': self.Config['client_secret'],
			'sandbox': True
		}
		self.GN = Gerencianet(options)		
		for key,Ticket in self.TicketsData.items():
			body = {'items':Ticket['items'],'shippings':Ticket['shippings']}
			NewTicket = self.GN.create_charge(body=body)
			if NewTicket['code'] == 200:
				ChargeId = NewTicket['data']['charge_id']
				Params = Ticket['params']
				Ticket['params'].update({'id':ChargeId})		
				NewIssue = self.GN.pay_charge(params=Ticket['params'], body={'payment':Ticket['payment']})
				if NewIssue['code'] == 200:
					Data = NewIssue['data']
					pdfkit.from_url(Data['link'], 'billing/tickets/' + str(ChargeId) + '.pdf')
					Data['path_billet'] = 'billing/tickets/' + str(ChargeId) + '.pdf'
					# print(Ticket['params'])	
					Ticket['params'].update(Data)
					# print(Ticket['params'])	
					Tickets.update({key:Ticket})
					self.deliver(Params)
		Tickets.save()
	def deliver(self,Data):
		print('sent')
		e = jinja2.Environment(extensions=["jinja2.ext.do",])
		t = e.from_string(self.Config['MessageContent'].decode("utf8"))
		Content = t.render(Data = Data)
		Mail = Mailer()
		Mail.addSend(Subject=self.Config['MessageTitle'].decode("utf8"),To=Data['ClientEmail'],Message=Content,PathAttach= 'billing/tickets/' +str(Data['charge_id']) + '.pdf', FileName=str(Data['charge_id']) + '.pdf' ,ToName=Data['ClinetName'])
		Mail.SendAll()