from utilities import keepdict
from datetime import datetime
from utilities import validar_cpf, validar_cnpj
from collections import OrderedDict
# from Lot import Lot
import json
class Rps(keepdict):
	NFConfig = keepdict('config/Nfe.json')
	AllRps = keepdict('nfe/xmls/rpss/Index.json')
	CreationStatus = None	
	ConfigRps = {}
	idRps = None
	Status = True
	ErrMessages = {}
	def __init__(self,idRps):
		self.idRps = str(idRps)
		if self.idRps in self.AllRps:
			self.ConfigRps = self.AllRps.get(self.idRps)
			super(Rps, self).__init__(self.ConfigRps['Path'])
			self.checkItems()
			self.CreationStatus = 'Old'
		else:
			self.CreationStatus = 'New'
			self.ConfigRps = {}
			self.ConfigRps['Status'] = 'notsent'
			self.ConfigRps['Path'] = 'nfe/xmls/rpss/' + self.ConfigRps['Status'] + '/' + self.idRps + '.xml'
			self.ConfigRps['idLot'] = 0
			self.AllRps.update({self.idRps:self.ConfigRps})
			self.AllRps.save()
			super(Rps, self).__init__(self.ConfigRps['Path'])
			Data = keepdict('nfe/xmls/models/RpsToSend.xml')
			Data['InfDeclaracaoPrestacaoServico']['@Id'] = self.idRps
			Data['InfDeclaracaoPrestacaoServico']['Rps']['@Id'] = self.idRps
			Data['InfDeclaracaoPrestacaoServico']['Rps']['IdentificacaoRps']['Numero'] = self.NFConfig.get('Rps').get('OptanteSimplesNacional')
			Data['InfDeclaracaoPrestacaoServico']['Rps']['IdentificacaoRps']['Serie'] = self.NFConfig.get('Rps').get('Serie')
			Data['InfDeclaracaoPrestacaoServico']['Rps']['IdentificacaoRps']['Tipo'] = self.NFConfig.get('Rps').get('Tipo')
			Data['InfDeclaracaoPrestacaoServico']['Prestador']['CpfCnpj']['Cnpj'] = self.NFConfig.get('Cnpj')
			Data['InfDeclaracaoPrestacaoServico']['Prestador']['InscricaoMunicipal'] = self.NFConfig.get('InscricaoMunicipal')
			Data['InfDeclaracaoPrestacaoServico']['RegimeEspecialTributacao'] = self.NFConfig.get('Rps').get('RegimeEspecialTributacao')
			Data['InfDeclaracaoPrestacaoServico']['OptanteSimplesNacional'] = self.NFConfig.get('Rps').get('OptanteSimplesNacional')
			Data['InfDeclaracaoPrestacaoServico']['IncentivoFiscal'] = self.NFConfig.get('Rps').get('OptanteSimplesNacional')
			self.update(Data)
			self.checkItems()
			super(Rps,self).save()

	def newRps(self,Cnpj = None):
		print(Cnpj)
	def setAttributes(self,ValorServicos = 0):
		print(ValorServicos)
	def setAttribute(self,Att,Value):
		print(Att)
	def round(self, Value):
		V = Value
		if not bool(Value):
			V = 0
		return "%.2f" % round(V,2)
	def save(self):
		self.checkItems()
		super(Rps,self).save()
	def checkItems(self):
		self.Status = True
		self.ErrMessages = {}
		for Key, Item in self.NFConfig['Items'].items():
			self.check(Key)
	def check(self,Key):
		Item = self.NFConfig.get('Items').get(Key)
		Value = self.getFromList(Item.get('ListPath'))
		checkType = self.checkType(Item['Type'],Value)
		if not checkType['Status']:
			self.Status = False
			self.ErrMessages.update({Key:checkType['Message']})
		elif Item['Required'] and not bool(checkType['Value']):
			self.Status = False
			self.ErrMessages.update({Key:"Field '" + Key + "' is required"})
	def setValues(self,RazaoSocial = None, CodigoMunicipio = None, ValorDeducoes = None, DataEmissao = None, DescontoCondicionado = None, Discriminacao = None, OutrasRetencoes = None, ValorCsll = None, Cnpj = None, Aliquota = None, Bairro = None, ValorPis = None, Numero = None, DescontoIncondicionado = None, ValorIr = None, Cpf = None, ValorCofins = None, ValorInss = None, Uf = None, ValorServicos = None, ValorIss = None, Competencia = None):
		if RazaoSocial is not None:
			self.setRazaoSocial(RazaoSocial)
		if CodigoMunicipio is not None:
			self.setCodigoMunicipio(CodigoMunicipio)
		if ValorDeducoes is not None:
			self.setValorDeducoes(ValorDeducoes)
		if DataEmissao is not None:
			self.setDataEmissao(DataEmissao)
		if DescontoCondicionado is not None:
			self.setDescontoCondicionado(DescontoCondicionado)
		if Discriminacao is not None:
			self.setDiscriminacao(Discriminacao)
		if OutrasRetencoes is not None:
			self.setOutrasRetencoes(OutrasRetencoes)
		if ValorCsll is not None:
			self.setValorCsll(ValorCsll)
		if Cnpj is not None:
			self.setCnpj(Cnpj)
		if Aliquota is not None:
			self.setAliquota(Aliquota)
		if Bairro is not None:
			self.setBairro(Bairro)
		if ValorPis is not None:
			self.setValorPis(ValorPis)
		if Numero is not None:
			self.setNumero(Numero)
		if DescontoIncondicionado is not None:
			self.setDescontoIncondicionado(DescontoIncondicionado)
		if ValorIr is not None:
			self.setValorIr(ValorIr)
		if Cpf is not None:
			self.setCpf(Cpf)
		if ValorCofins is not None:
			self.setValorCofins(ValorCofins)
		if ValorInss is not None:
			self.setValorInss(ValorInss)
		if Uf is not None:
			self.setUf(Uf)
		if ValorServicos is not None:
			self.setValorServicos(ValorServicos)
		if ValorIss is not None:
			self.setValorIss(ValorIss)
		if Competencia is not None:
			self.setCompetencia(Competencia)
	def setRazaoSocial(self,RazaoSocial):
		self.setValue('RazaoSocial',RazaoSocial)
	def setCodigoMunicipio(self,CodigoMunicipio):
		self.setValue('CodigoMunicipio',CodigoMunicipio)
	def setValorDeducoes(self,ValorDeducoes):
		self.setValue('ValorDeducoes',ValorDeducoes)
	def setDataEmissao(self,DataEmissao):
		self.setValue('DataEmissao',DataEmissao)
	def setDescontoCondicionado(self,DescontoCondicionado):
		self.setValue('DescontoCondicionado',DescontoCondicionado)
	def setDiscriminacao(self,Discriminacao):
		self.setValue('Discriminacao',Discriminacao)
	def setOutrasRetencoes(self,OutrasRetencoes):
		self.setValue('OutrasRetencoes',OutrasRetencoes)
	def setValorCsll(self,ValorCsll):
		self.setValue('ValorCsll',ValorCsll)
	def setCnpj(self,Cnpj):
		self.setValue('Cnpj',Cnpj)
	def setAliquota(self,Aliquota):
		self.setValue('Aliquota',Aliquota)
	def setBairro(self,Bairro):
		self.setValue('Bairro',Bairro)
	def setValorPis(self,ValorPis):
		self.setValue('ValorPis',ValorPis)
	def setNumero(self,Numero):
		self.setValue('Numero',Numero)
	def setDescontoIncondicionado(self,DescontoIncondicionado):
		self.setValue('DescontoIncondicionado',DescontoIncondicionado)
	def setValorIr(self,ValorIr):
		self.setValue('ValorIr',ValorIr)
	def setCpf(self,Cpf):
		self.setValue('Cpf',Cpf)
	def setValorCofins(self,ValorCofins):
		self.setValue('ValorCofins',ValorCofins)
	def setValorInss(self,ValorInss):
		self.setValue('ValorInss',ValorInss)
	def setUf(self,Uf):
		self.setValue('Uf',Uf)
	def setValorServicos(self,ValorServicos):
		self.setValue('ValorServicos',ValorServicos)
	def setValorIss(self,ValorIss):
		self.setValue('ValorIss',ValorIss)
	def setCompetencia(self,Competencia):
		self.setValue('Competencia',Competencia)	
	def setValue(self,Key,Value):
		Item = self.NFConfig.get('Items').get(Key)
		ListPath = Item['ListPath'][:]
		ReversedList = Item['ListPath'][:]
		ReversedList.reverse()
		NewData = {}
		for i in range(len(ReversedList)):
			if i == 0:
				NewList = ListPath[:len(ListPath) - (i + 1)]
				NewData = self.getFromList(NewList)
				NewData.update({ReversedList[i]:self.mask(Item.get('Type'),Value)})
			elif i == len(ListPath) -1:
				self.update({ListPath[0]:NewData})
			else:
				OldData = NewData
				NewList = ListPath[:len(ListPath) - (i + 1)]
				NewData = self.getFromList(NewList)
				NewData.update({ReversedList[i]:OldData})
		self.checkItems()
	def mask(self,Type,Value):
		if Type == 'string':
			return str(Value)
		elif Type == 'float':
			try:
				v = float(Value)
			except Exception as e:
				v = 0
			return "%.2f" % (v)
		elif Type == 'cnpj':
			try:
				v = int(Value)
			except Exception as e:
				v = 0
			return "%014d" % (v)
		elif Type == 'cpf':
			try:
				v = int(Value)
			except Exception as e:
				v = 0
			return "%011d" % (v)
		elif Type == 'cpf':
			try:
				v = int(Value)
			except Exception as e:
				v = 0
			return "%011d" % (v)	
		elif Type == 'date':
			try:	
				ValueData = datetime.strptime(Value, "%Y-%m-%d")
			except Exception as e:
				ValueData = datetime.strptime('2017-01-01', "%Y-%m-%d")
			return ValueData.strftime("%Y-%m-%d")
		else:
			print(Type)
	def getFromList(self,ListPath):
		Value = self
		if not bool(ListPath):
			return None
		if type(ListPath) != list:
			return None
		for i in range(len(ListPath)):
			if not bool(Value):
				return None
			Value = Value.get(ListPath[i])
		if type(Value) == OrderedDict:
			return Value
		elif bool(Value):
			return str(Value).encode('utf-8')
		else:
			return Value
	def checkType(self,Type,Value):
		Return = {'Status':True,'Message':'','Value':Value}
		if Type == 'int':
			try:
				Return['Value'] = int(Value)
			except Exception as e:
				Return['Status'] = False
				Return['Message'] = "Value '" + str(Value) + "' is not " + Type
		elif Type == 'float':
			try:
				Return['Value'] = float(Value)
			except Exception as e:
				Return['Status'] = False
				Return['Message'] = "Value '" + str(Value) + "' is not a '" + Type + "' valid!"
		elif Type == 'string':
			try:
				Return['Value'] = str(Value)
			except Exception as e:
				Return['Status'] = False
				Return['Message'] = "Value '" + str(Value) + "' is not a '" + Type + "' valid!"
		elif Type == 'date':
			InitialData = datetime.strptime('2017-01-01', "%Y-%m-%d")
			try:	
				ValueData = datetime.strptime(Value, "%Y-%m-%d")			
				if (ValueData < datetime.strptime('2017-01-01', "%Y-%m-%d")):
					Return['Status'] = False
					Return['Value'] = ValueData.strftime("%Y-%m-%d")
					Return['Message'] = "Value '" + str(Value) + "' is not a '" + Type + "' valid!"
			except Exception as e:
				Return['Status'] = False
				Return['Value'] = '0001-01-01'
				Return['Message'] = "Value '" + str(Value) + "' is not a '" + Type + "' valid!"
		elif Type == 'cnpj':
			if validar_cnpj(Value):
				Return['Value'] = Value
			else:
				Return['Message'] = "invalid Cnpj"
			Return['Value'] = Value
		elif Type == 'cpf':
			if validar_cpf(Value):
				Return['Value'] = Value
			else:
				Return['Message'] = "invalid Cpf"
			Return['Value'] = Value			
		else:
			print('type',Type)
		return Return
	def ViewDict(self,Dict):
		return json.dumps(Dict,indent=4,sort_keys=True)	
	def ViewErrs(self):
		return json.dumps(self.ErrMessages,indent=4,sort_keys=True)	
	def attach(self,idLot):
		if bool(idLot):
			self.ConfigRps['idLot'] = idLot
			self.AllRps.update({self.idRps:self.ConfigRps})
			self.AllRps.save()			
			return True
		else:
			return False
	def archive(self):
		if self.AllRps[self.idRps]['Status'] != 'archived':
			self.AllRps[self.idRps]['Status'] = 'archived'
			self.move(3,self.AllRps[self.idRps]['Status'])
			self.AllRps[self.idRps]['Path'] = self.getPath()
			self.AllRps.save()
			return True
		else:
			return False
	def faile(self):
		if self.AllRps[self.idRps]['Status'] != 'failed':
			self.AllRps[self.idRps]['Status'] = 'failed'
			self.move(3,self.AllRps[self.idRps]['Status'])
			self.AllRps[self.idRps]['Path'] = self.getPath()
			self.AllRps.save()
			return True
		else:
			return False
	def notsend(self):
		if self.AllRps[self.idRps]['Status'] != 'notsent':
			self.AllRps[self.idRps]['Status'] = 'notsent'
			self.move(3,self.AllRps[self.idRps]['Status'])
			self.AllRps[self.idRps]['Path'] = self.getPath()
			self.AllRps.save()
			return True
		else:
			return False			
	def send(self):
		if self.AllRps[self.idRps]['Status'] != 'sent':
			self.AllRps[self.idRps]['Status'] = 'sent'
			self.move(3,self.AllRps[self.idRps]['Status'])
			self.AllRps[self.idRps]['Path'] = self.getPath()
			self.AllRps.save()
			return True
		else:
			return False