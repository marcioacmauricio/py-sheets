# -*- coding: utf-8 -*-
from utilities import keepdict
from Rps import Rps
class Lot(keepdict):
	NFConfig = keepdict('config/Nfe.json')
	AllLots = keepdict('nfe/xmls/lots/Index.json')
	CreationStatus = None
	ConfigLot = {}
	idLot = ''
	def __init__(self, idLot):
		self.idLot = str(idLot)
		if self.idLot in self.AllLots:
			self.CreationStatus = 'Old'
			self.ConfigLot = self.AllLots.get(self.idLot)
			super(Lot, self).__init__(self.ConfigLot['Path'])
		else:
			self.CreationStatus = 'New'
			self.ConfigLot = {}
			self.ConfigLot['Status'] = 'notsent'
			self.ConfigLot['Path'] = 'nfe/xmls/lots/' + self.ConfigLot['Status'] + '/' + self.idLot + '.xml'
			self.ConfigLot['Rpss'] = []
			self.AllLots.update({self.idLot:self.ConfigLot})
			self.AllLots.save()
			super(Lot, self).__init__(self.ConfigLot['Path'])
			Data = keepdict('nfe/xmls/models/EnviarLoteRpsEnvio.xml')
			Data['EnviarLoteRpsEnvio']['LoteRps']['@Id'] = self.idLot
			Data['EnviarLoteRpsEnvio']['LoteRps']['CpfCnpj']['Cnpj'] = self.NFConfig['Cnpj']
			Data['EnviarLoteRpsEnvio']['LoteRps']['InscricaoMunicipal'] = self.NFConfig['InscricaoMunicipal']
			Data['EnviarLoteRpsEnvio']['LoteRps']['NumeroLote'] = self.idLot
			self.update(Data)		
		Signed = keepdict('nfe/xmls/models/Signed.xml')
		self['EnviarLoteRpsEnvio'].update(Signed)
		self.save()
	def attachRps(self,idRps):
		RPS = Rps(idRps)
		if not RPS.Status:
			return False
		else:
			if idRps not in self.AllLots[self.idLot]['Rpss']:
				RPS.attach(self.idLot)
				self.AllLots[self.idLot]['Rpss'].append(idRps)
				self.AllLots.save()
				self.save()
				return True
	def save(self):
		Rpss = []
		for i in range(len(self.ConfigLot['Rpss'])):
			Rpss.append(Rps(self.ConfigLot['Rpss'][i]))
		self['EnviarLoteRpsEnvio']['LoteRps'].update({'ListaRps':{'Rps':Rpss}})
		super(Lot,self).save()
	def detachRps(self,idRps):
		print(idRps)
	def archive(self):
		if self.AllLots[self.idLot]['Status'] != 'archived':
			self.AllLots[self.idLot]['Status'] = 'archived'
			self.move(3,self.AllLots[self.idLot]['Status'])
			self.AllLots[self.idLot]['Path'] = self.getPath()
			self.AllLots.save()
			for i in range(len(self.AllLots[self.idLot]['Rpss'])):
				idRps = self.AllLots[self.idLot]['Rpss'][i]
				Rps(idRps).archive()
			return True
		else:
			return False
	def faile(self):
		if self.AllLots[self.idLot]['Status'] != 'failed':
			self.AllLots[self.idLot]['Status'] = 'failed'
			self.move(3,self.AllLots[self.idLot]['Status'])
			self.AllLots[self.idLot]['Path'] = self.getPath()
			self.AllLots.save()
			for i in range(len(self.AllLots[self.idLot]['Rpss'])):
				idRps = self.AllLots[self.idLot]['Rpss'][i]
				Rps(idRps).faile()
			return True
		else:
			return False
	def notsend(self):
		if self.AllLots[self.idLot]['Status'] != 'notsent':
			self.AllLots[self.idLot]['Status'] = 'notsent'
			self.move(3,self.AllLots[self.idLot]['Status'])
			self.AllLots[self.idLot]['Path'] = self.getPath()
			self.AllLots.save()
			for i in range(len(self.AllLots[self.idLot]['Rpss'])):
				idRps = self.AllLots[self.idLot]['Rpss'][i]
				Rps(idRps).notsend()
			return True
		else:
			return False			
	def send(self):
		if self.AllLots[self.idLot]['Status'] != 'sent':
			self.AllLots[self.idLot]['Status'] = 'sent'
			self.move(3,self.AllLots[self.idLot]['Status'])
			self.AllLots[self.idLot]['Path'] = self.getPath()
			self.AllLots.save()
			for i in range(len(self.AllLots[self.idLot]['Rpss'])):
				idRps = self.AllLots[self.idLot]['Rpss'][i]
				Rps(idRps).send()			
			return True
		else:
			return False
