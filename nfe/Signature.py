from utilities import keepdict
class Signature:
	ConfigJson = keepdict('config/Signature.json')
	Signature = keepdict('nfe/xmls/models/Signature.xml')
	def get(self):
		self.Signature['Signature']['SignedInfo']['Reference']['DigestValue'] = self.ConfigJson['DigestValue']
		self.Signature['Signature']['SignatureValue'] = self.ConfigJson['SignatureValue']
		self.Signature['Signature']['KeyInfo']['X509Data']['X509Certificate'] = self.ConfigJson['X509Certificate']
		Signed = keepdict('nfe/xmls/models/Signed.xml')
		Signed.update(self.Signature)
		Signed.save()


		