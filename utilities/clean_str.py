# -*- coding: utf-8 -*-
from unicodedata import normalize
def clean_str(txt, codif='utf-8'):
	return normalize('NFKD', txt.decode(codif)).encode('ASCII','ignore')
