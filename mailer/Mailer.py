# -*- coding: utf-8 -*-
from utilities import keepdict
import smtplib
import mimetypes
import email
import email.mime.application
class Mailer:
	Config = keepdict('config/Mailer.json')
	Sends = []
	Title = ''
	def addSend(self, Subject, To, Message, PathAttach = None,FileName = None, ToName = None):
		Data = keepdict('mailer/Sends.json')
		Data.update(self.Config)
		Send = {}
		Send['Subject'] = Subject
		Send['To'] = To
		Send['Message'] = Message
		Send['PathAttach'] = PathAttach
		Send['FileName'] = FileName
		Send['ToName'] = ToName
		self.Sends.append(Send)
		Data['Sends'] = self.Sends
		Data.save()
		return True
	def getMessage(self,Message):
		Msg = email.mime.Multipart.MIMEMultipart()
		Msg['Subject'] = Message['Subject']
		Msg['From'] = self.Config['FromName'] + ' <' + self.Config['FromAddr'] + '>'
		Msg['To'] = Message['To']
		body = email.mime.Text.MIMEText(str(Message['Message'].encode('utf-8')))
		Msg.attach(body)
		File = open( Message['PathAttach'], 'rb')
		Attachment = email.mime.application.MIMEApplication(File.read(),_subtype="pdf")
		File.close()
		Attachment.add_header('Content-Disposition','attachment',filename=Message['FileName'])
		Msg.attach(Attachment)
		return Msg
	def SendAll(self):
		Data = keepdict('mailer/Sends.json')
		ServerSmpt = self.Config['SmtpServer'] + ":" + str(self.Config['Port'])
		s = smtplib.SMTP(ServerSmpt)
		s.starttls()
		s.login(self.Config['FromAddr'],self.Config['Password'])
		for key in range(len(Data['Sends'])):
			Msg = self.getMessage(Data['Sends'][key])
			s.sendmail(self.Config['FromAddr'],[Msg['To']], Msg.as_string())
		s.quit()
		Data.empty()
		# print(Data.pretty())
	def sendAccumulatedEmails(self):
		print('sendAccumulatedEmails')
		