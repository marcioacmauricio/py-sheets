from utilities import keepdict
from utilities import pretty
import string
import json
import httplib2
import os
from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
import time
try:
	import argparse
	flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
	flags = None
class Sheet:
	SCOPES = None
	CLIENT_SECRET_FILE = None
	APPLICATION_NAME = None
	SHEET_KEY = None
	SHEET_NAME = None
	discoveryUrl = None
	ColumnIni = None
	ColumnEnd = None
	LineIni = 1
	LineEnd = None
	CellIni = None
	CellEnd = None
	ViewData = {}
	UpdateData = []
	def __init__(self):
		self.Config = keepdict('config/Sheet.json')
		self.SCOPES = self.Config['SCOPES']
		self.CLIENT_SECRET_FILE = self.Config['CLIENT_SECRET_FILE']
		self.APPLICATION_NAME = self.Config['APPLICATION_NAME']
		self.discoveryUrl = self.Config['DISCOVERY_URL']
		self.SHEET_KEY = self.Config['SHEET_KEY']
		# print(self.Config.pretty())
	def setSheetName(self,SheetName):
		self.SHEET_NAME = SheetName
	def getCredentials(self):
		home_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
		credential_dir = os.path.join(home_dir, 'credentials')
		if not os.path.exists(credential_dir):
			os.makedirs(credential_dir)
		credential_path = os.path.join(credential_dir,'sheets.googleapis.com-python-quickstart.json')
		store = Storage(credential_path)
		credentials = store.get()
		if not credentials or credentials.invalid:
			flow = client.flow_from_clientsecrets(self.CLIENT_SECRET_FILE, self.SCOPES)
			flow.user_agent = self.APPLICATION_NAME
			if flags:
				credentials = tools.run_flow(flow, store, flags)
			else: # Needed only for compatibility with Python 2.6
				credentials = tools.run(flow, store)
		return credentials		
	def getService(self):
		credentials = self.getCredentials()
		http = credentials.authorize(httplib2.Http())
		return discovery.build('sheets', 'v4', http=http, discoveryServiceUrl=self.discoveryUrl)
	def processPayment(self,Values):
		dateTime = time.strftime("%d/%m/%Y")
		newVals = []
		for i in range(len(Values)):
			newVal = []
			if (Values[i][0] == 'Faturar'):
				newVal.append('Faturado')
				newVal.append(dateTime)
			else:
				newVal.append(Values[i][0])
				if len(Values[i]) > 1:
					newVal.append(Values[i][1])
				else:
					newVal.append("")
			newVals.append(newVal)
		return newVals		
	def setColumns(self,ColumnIni,ColumnEnd):
		self.ColumnIni = ColumnIni.upper()
		self.ColumnEnd = ColumnEnd.upper()
		if bool(self.LineIni):
			self.CellIni = self.ColumnIni + str(self.LineIni)

		if bool(self.LineEnd):
			self.CellEnd = self.ColumnEnd + str(self.LineEnd)
		else:
			self.CellEnd = self.ColumnEnd
	def setLines(self,LineIni,LineEnd):
		self.LineIni = LineIni
		self.LineEnd = LineEnd
		if bool(self.ColumnIni):
			self.CellIni = self.ColumnIni + str(self.LineIni)

		if bool(self.ColumnEnd):
			self.CellEnd = self.ColumnEnd + str(self.LineEnd)
	def LetterToNumber(self,Str):
		Letther = string.uppercase
		Index = Letther.index(Str[0])
		return Index
	def NumberToLetther(self,Key):
		Letther = string.uppercase
		return Letther[Key]	
	def getLine(self,Value):
		numColumnIni = self.LetterToNumber(self.ColumnIni)
		numColumnEnd = self.LetterToNumber(self.ColumnEnd)
		Lenght = numColumnEnd - numColumnIni + 1
		Return = {}
		for key in range(Lenght):
			IndexNumber = key + numColumnIni
			IndexLetther = self.NumberToLetther(IndexNumber)			
			if key >= len(Value):
				Return.update({IndexLetther:''})
			else:
				Return.update({IndexLetther:Value[key]})
				
		return Return
	def setData(self):
		if not bool(self.CellIni) or not bool(self.CellEnd):
			Msg = "Columns not set!"
			raise ValueError(Msg)
		rangeName = self.SHEET_NAME + '!' + self.CellIni + ':' + self.CellEnd
		service = self.getService()
		result = service.spreadsheets().values().get( spreadsheetId=self.SHEET_KEY, range=rangeName).execute()		
		# Data = keepdict('sheets/ViewData.json')
		if bool(result):
			self.ViewData['ColumnIni'] = self.ColumnIni
			self.ViewData['ColumnEnd'] = self.ColumnEnd
			self.ViewData['LineIni'] = self.LineIni
			self.ViewData['LineEnd'] = self.LineEnd
			self.ViewData['SheetKey'] = self.SHEET_KEY
			self.ViewData['SheetName'] = self.SHEET_NAME
			self.ViewData['range'] = str(result['range'])
			self.ViewData['majorDimension'] = str(result['majorDimension'])
			self.ViewData['values'] = {}
			for i in range(len(result['values'])):
				value = result['values'][i]
				self.ViewData['values'].update({self.LineIni + i:self.getLine(value)})
		# Data.update(self.ViewData)
		# Data.save()
		# Data.empty()
	def getData(self):
		return self.ViewData		
	def setCellToUpdate(self,Tab, ColumnName,LineNumber,Value):
		Data = keepdict('sheets/UpdateData.json')
		Data['SheetKey'] = self.SHEET_KEY
		Data['SheetName'] = self.SHEET_NAME
		NewRange = {}
		NewRange['range'] = Tab + '!' + ColumnName.upper() + str(LineNumber)
		NewRange['values'] = [[Value]]
		self.UpdateData.append(NewRange)
		Data['ranges'] = self.UpdateData
		Data.save()
	def UpdateCells(self):
		Data = keepdict('sheets/UpdateData.json')
		if 'ranges' in Data:
			body = {
				'valueInputOption': 'USER_ENTERED',
				'data': Data['ranges']
			}
			service = self.getService()
			result = service.spreadsheets().values().batchUpdate(spreadsheetId=self.SHEET_KEY, body=body).execute()
			Data.empty()
	def getTab(self,Values):
		Range = Values.get('range')
		Return = {}
		if not bool(Range):
			return {}
		Return['Tab'] = Range.split('!')[0]
		Vals = Values.get('values')
		Return['rowCount'] = len(Vals)
		Return['values'] = {}
		if bool(Return['rowCount']):
			Return['columnCount'] = len(Vals[0])
			ColumNames = Vals[0]
			for i in range(len(Vals)):
				Val = Vals[i]
				if i == 0:
					Keys = {}
					for j in range(Return['columnCount']):
						Keys[Val[j]] = self.NumberToLetther(j)
					Return['keys'] = Keys
				else:
					NewVal = {}
					NewVal['Id'] = str(i + 1)
					for j in range(Return['columnCount']):
						Column = ColumNames[j]
						if j >= len(Val):
							NewVal[Column] = ''
						else:
							NewVal[Column] = str(Val[j]).encode('utf-8')
					Return['values'][str(i + 1)] = NewVal 
		return Return
	def getAllData(self):
		Data = keepdict('sheets/FullData.json')
		Data.empty()
		service = self.getService()
		range_names = ['Registros','Clientes','DB','Configuracao','Status','Fatura']
		result = service.spreadsheets().values().batchGet(
			spreadsheetId=self.SHEET_KEY, ranges=range_names).execute()
		Data.update(result)
		Data.save()	
		valueRanges = Data.get('valueRanges')
		AllTabs = keepdict('sheets/AllTabs.json')
		AllTabs.empty()
		if bool(valueRanges):
			for i in range(len(valueRanges)):
				Tab = self.getTab(valueRanges[i])
				AllTabs.update({Tab['Tab']:Tab})
		AllTabs.save()
		print('as')
		return AllTabs


			



