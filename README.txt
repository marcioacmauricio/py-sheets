sudo yum install wkhtmltopdf

sudo pip install -r requirements.txt

usage:

[marcio@leptop py-sheets]$ pysheets -h
usage: pysheets [-h] [-c] [-b] [-n] [-m] [-B] [-N]

optional arguments:
  -h, --help            show this help message and exit
  -c, --configure       set initial settings
  -b, --billing         issue accumulated charges
  -n, --nfe             bill accumulated charges
  -m, --mailer          send accumulated emails
  -B, --notify-billing  confirm accumulated receipt
  -N, --notify-nfe      confirm invoice issuance


Opções


--configure

Opção a ser ustada apenas para a primeira utilização.


Solicita o ID da planilha principal no Google SpreadSheets, a senha do e-mail ou alguma outra informação confidencial que não possa ser definida através da guia de configuração da planilha principal.

Logo após irá exibir uma URL que deverá ser acessada pelo administrador para autorização da conexão 

--billing 

Processa a planilha de pedidos, emite ou cancelar boletos, agrupando para envio por e-mail, conforme status dos pedidos

--nfe

Processa as cobranças emitidas, avalia e emite RPS provisório, agrupando em lotes e enviando para emissão de notas fiscais.

--mailer 

Verifica a lista de e-mail com envio pendente e entrega as mensagens de cobrança e notas fiscais

--notify-billing 

Consulta o gateway de pagamentos e atualiza o recebimento  da fatura


--notify-nfe

Consulta os lotes emitidos no servidores da receita, e associa à fatura, atualizando o numero da nota


As opções podem ser utilizadas emconjunto 
[marcio@leptop py-sheets]$ pysheets -bnmBN
 fazendo todas operações de forma sequencial, 
 ou cada opção pode ser invocada separadamente com 
 [marcio@leptop py-sheets]$ ./pysheets -c

