# -*- coding: utf-8 -*-
from config import Config
from utilities import keepdict, pretty, GroupBy
from brazilnum.cnpj import validate_cnpj
from brazilnum.cpf import validate_cpf
from brazilnum.muni import validate_muni
from validate_email import validate_email
from sheets import Sheet
from Registros import Registros
import sys
from datetime import datetime, timedelta, date
import re
from nfe import Lot
class pySheets(keepdict):
	Sandbox = None
	UpdateConfig = None
	BlockAgent = None
	Sheet = Sheet()
	Lot = None
	def __init__(self,isConstruct = False):
		if isConstruct:
			super(pySheets, self).__init__('sheets/AllTabs.json')
			if not bool(self):
				self.update(self.Sheet.getAllData())
				self.save()
			Values = self.get('Status').get('values')
			try:
				self.Sandbox = bool(int(Values['4'].get('Valor')))
			except Exception as e:
				self.Sandbox = True
			try:
				self.UpdateConfig = bool(int(Values['3'].get('Valor')))
			except Exception as e:
				self.UpdateConfig = False
			try:
				self.BlockAgent = bool(int(Values['2'].get('Valor')))
			except Exception as e:
				self.BlockAgent = True				
	def Configure(self):
		ConfigSheet = keepdict('config/Sheet.json')
		SHEET_KEY = raw_input("Paste Spread Sheets Key (ctrl + shift + v): ")
		ConfigSheet.update({'SHEET_KEY':SHEET_KEY})
		ConfigSheet.save()
		ConfigMailer = keepdict('config/Mailer.json')
		Password = raw_input("Paste Password Mailer (ctrl + shift + v): ")
		ConfigMailer.update({'Password':Password})
		ConfigMailer.save()	
		S = Sheet()
		S.setSheetName('Configuracao')
		S.setColumns('A','B')
		S.setLines(1,45)
		S.setData()
		DataConfig = S.getData()
		C = Config()
		C.Configure(DataConfig)
		S.setSheetName('Status')
		S.setCellToUpdate('B',2,'0')
		S.UpdateCells()		
		sys.exit()
	def chargeNewOrders(self):
		if self.UpdateConfig:
			C = Config()
			C.Configure(Data.get('Configuracao'))
			S = Sheet()
			S.setSheetName('Status')
			S.setCellToUpdate('B',3,'0')
			S.UpdateCells()
		if self.BlockAgent:
			byStatus = self.GroupBy('Registros','Status')
			Faturar = byStatus.get('Faturar')
			if bool(Faturar):
				self.Faturar(Faturar)
			Reemitir = byStatus.get('Reemitir')
			if bool(Reemitir):
				self.Reemitir(Reemitir)
			Cancelar = byStatus.get('Cancelar')
			if bool(Cancelar):
				self.Cancelar(Cancelar)
	def Faturar(self,Faturar):
		byTomador = self.GroupBy(Faturar,'Tomador')
		for Key, Value in byTomador.items():
			Clientes = self.Where('Clientes','Nome',Key)
			Cliente = {}
			if bool(Clientes):
				ClienteId = list(Clientes.keys())[0]
				Cliente = Clientes[ClienteId]
			if bool(Cliente) and bool(Faturar):
				self.FaturarCliente(Faturar,Cliente)
	def FaturarCliente(self,Faturar,Cliente):
		try:
			Antecedencia = int(Cliente.get('Antecedencia'))
		except Exception as e:
			Antecedencia = 0
		byDate = GroupBy(Faturar,'DataVencimento')
		for K, V in byDate.items():
			ValueData = datetime.strptime(K, "%d/%m/%Y")
			Intervalo = timedelta(Antecedencia)
			DataEmissao = ValueData - Intervalo
			if (datetime.today() < ValueData):
				IssueInvoice = self.IssueInvoice(Cliente,V)
				
	def CheckClient(self,Cliente):
		Return = {}
		Return['Status'] = True
		Return['Message'] = ""
		try:
			Antecedencia = int(Cliente.get('Antecedencia'))
		except Exception as e:
			Antecedencia = 0
		if not bool(Antecedencia):
			Return['Status'] = False
			Return['Message'] += "\n" + "Erro em 'Antecedencia'"
		fil = re.compile('([0-9]+)')
		resp = fil.findall(Cliente.get('Cep'))
		if bool(resp):
			if len(resp[0]) != 8:
				Return['Status'] = False
				Return['Message'] += "\n" + "Erro em 'Cep'"
		else:
			Return['Status'] = False
			Return['Message'] += "\n" + "Erro em 'Cep'"
		if not bool(Cliente.get('Cidade')):
			Return['Status'] = False
			Return['Message'] += "\n" + "Erro em 'Cidade'"
		if Cliente.get('SujeitoAtivo') == 'PJ':
			if not validate_cnpj(Cliente.get('CpfCnpj')):
				Return['Status'] = False
				Return['Message'] += "\n" + "Erro em 'CpfCnpj'"
		elif Cliente.get('SujeitoAtivo') == 'PF':
			if not validate_cpf(Cliente.get('CpfCnpj')):
				Return['Status'] = False
				Return['Message'] += "\n" + "Erro em 'CpfCnpj'"
		else:
			Return['Status'] = False
			Return['Message'] += "\n" + "Erro em 'CpfCnpj'"
		if not validate_email(Cliente.get('EmailEnvio'),check_mx=True):
			Return['Status'] = False
			Return['Message'] += "\n" + "Erro em 'EmailEnvio'"
		if not bool(Cliente.get('Cidade')):
			Return['Status'] = False
			Return['Message'] += "\n" + "Erro em 'Cidade'"
		if not validate_muni(Cliente.get('CodigoMunicipio')):
			Return['Status'] = False
			Return['Message'] += "\n" + "Erro em 'CodigoMunicipio'"
		if not bool(Cliente.get('Endereco')):
			Return['Status'] = False
			Return['Message'] += "\n" + "Erro em 'Endereco'"
		if not bool(Cliente.get('Nome')):
			Return['Status'] = False
			Return['Message'] += "\n" + "Erro em 'Nome'"
		if not bool(Cliente.get('Numero')):
			Return['Status'] = False
			Return['Message'] += "\n" + "Erro em 'Numero'"
		if not bool(Cliente.get('RazaoSocial')):
			Return['Status'] = False
			Return['Message'] += "\n" + "Erro em 'RazaoSocial'"
		if not bool(Cliente.get('SujeitoAtivo')):
			Return['Status'] = False
			Return['Message'] += "\n" + "Erro em 'SujeitoAtivo'"
		if not bool(Cliente.get('Uf')):
			Return['Status'] = False
			Return['Message'] += "\n" + "Erro em 'Uf'"		
		return Return
	def IssueInvoice(self,Cliente,Items):
		CheckClient = self.CheckClient(Cliente)
		# if not bool(CheckClient.get('Status')):
		self.UpdateCell('Clientes','ErroMensagem',Cliente.get('Id'),CheckClient.get('Message'))
		Discriminacao = ""
		Value = 0.0
		Stt = True
		for KeyItem,Item in Items.items():
			self.UpdateCell('Registros','Status',Item.get('Id'),'Faturado')
			self.UpdateCell('Registros','Fatura',Item.get('Id'),'1')
			try:
				Value += float(Item.get('ValorServicos').replace(',','.'))
			except Exception as e:
				Stt = False
			if bool(Discriminacao):
				Discriminacao += "\n" + Item['DescricaoEstruturada']
			else:
				Discriminacao += Item['DescricaoEstruturada']
		self.UpdateCell('Fatura','Numero','2','1')
		self.UpdateCell('Fatura','ValorServicos','2',Value)
		self.UpdateCell('Fatura','RazaoSocial','2',Cliente.get('Nome'))
		self.UpdateCell('Fatura','Discriminacao','2',Discriminacao)
		# print(pretty(Cliente))
	def UpdateCell(self,Tab,Column,Row,Value):
		TabData = self.get(Tab)
		if not bool(TabData):
			return False
		ColumnUpdate = TabData['keys'].get(Column)
		if not bool(ColumnUpdate):
			return False
		if not bool(Row):
			return False
		self.Sheet.setCellToUpdate(Tab,ColumnUpdate,Row,Value)
	def Reemitir(self,Cancelar):
		print(Cancelar)
	def Cancelar(self,Cancelar):
		print(Cancelar)
	def Where(self,Tab,Field,Value):
		DataTab = self.get(Tab)
		Return = {}
		if not bool(DataTab):
			return Return
		Values = DataTab.get('values')
		if not bool(Values):
			return Return
		for Key, Val in Values.items():
			ValueField = Val.get(Field)
			if bool(ValueField):
				if ValueField == Value:
					Return.update({Key:Val})
		return Return
	def GroupBy(self,Tab,Field):
		Return = {}
		V = self.get('Registros')
		Values = V.get('values')
		if bool(Values):
			Return = GroupBy(Values,Field)
		return Return
	def billAccumulatedCharges(self):
		print('billAccumulatedCharges')
	def sendAccumulatedEmails(self):
		print('sendAccumulatedEmails')
	def confirmAccumulatedReceipt(self):
		print('confirmAccumulatedReceipt')
	def confirmInvoiceIssuance(self):
		print('confirmInvoiceIssuance')	
	def close(self):
		self.empty()
		self.Sheet.UpdateCells()
		print('die')