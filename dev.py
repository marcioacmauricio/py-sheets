#!/usr/bin/python
# -*- coding: utf-8 -*-
import tornado.ioloop
import tornado.web
import sys
from main import main
main()
class MainHandler(tornado.web.RequestHandler):
	def get(self):
		self.write("Develop py Sheet")

if __name__ == "__main__":
	application = tornado.web.Application([
		(r"/", MainHandler),
	],debug=True)
	application.listen(8888)
	tornado.ioloop.IOLoop.current().start()
