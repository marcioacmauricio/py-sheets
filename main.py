# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from sheets import Sheet
from mailer import Mailer
from billing import GerenciaNet
from billing import Billing
from utilities import keepdict
from nfe import Nfe
from nfe import Signature
from pySheets import pySheets 
import sys
reload(sys)
sys.setdefaultencoding('UTF-8')

def main():
	S = pySheets(True)
	S.chargeNewOrders()
	S.close()
	# # -> set config sheet <-
	# ConfigSheet = keepdict('config/Sheet.json')	
	# ConfigSheet['SCOPES'] = 'https://www.googleapis.com/auth/spreadsheets'
	# ConfigSheet['CLIENT_SECRET_FILE'] = 'config/client_secret.json'
	# ConfigSheet['APPLICATION_NAME'] = 'Google Sheets API Python Quickstart'
	# ConfigSheet['DISCOVERY_URL'] = 'https://sheets.googleapis.com/$discovery/rest?version=v4'
	# ConfigSheet['SHEET_KEY'] = '1uRs-QMdjBFe_EGWg5jlbFysd8lhAoPPm1NHpF_gGYWE'
	# ConfigSheet.save()
	# print(ConfigSheet.pretty())


	# # -> get values in range cells <-
	# sheet = Sheet('Sheet1')	
	# sheet.setColumns('A','E')
	# sheet.setLines(1,17)
	# sheet.setData()
	# print(sheet.getData())

	# # -> set cells to update <-
	# sheet = Sheet('Sheet1')	
	# sheet.setCellToUpdate('b',10,'A faturar')
	# sheet.setCellToUpdate('b',11,'A faturar')
	# sheet.UpdateCells()


	# # -> set config email <-
	# ConfigMailer = keepdict('config/Mailer.json')
	# ConfigMailer['SmtpServer'] = 'smtp.gmail.com'
	# ConfigMailer['Port'] = 587
	# ConfigMailer['FromAddr'] = 'py.spreadsheets@gmail.com'
	# ConfigMailer['FromName'] = 'Contato'
	# ConfigMailer['Password'] = 'PySpreadsheets123'	
	# print(ConfigMailer.pretty())
	# ConfigMailer.save()


	# # -> send mail test <-
	# Mail = Mailer()
	# Mail.addSend(Subject="Teste",To="marcioacmauricio@gmail.com",Message="conteúdo de teste",PathAttach='mailer/attachment/Teste.pdf',FileName = 'Teste.pdf',ToName='Marcio Maurício')
	# Mail.addSend(Subject="Teste",To="marciomau@gmail.com",Message="conteúdo de teste",PathAttach='mailer/attachment/Teste.pdf',FileName = 'Teste.pdf',ToName='Marcio Maurício')
	# Mail.SendAll()


	# -> set config ticket <-
	# ConfigTicket = keepdict('config/Ticket.json')
	# ConfigTicket['Gateway'] = 'GerenciaNet'
	# ConfigTicket['client_id'] = 'Client_Id_02045c9fd4ec659c1694baa546318dfb9750405b'
	# ConfigTicket['client_secret'] = 'Client_Secret_0cd256255998b36550829652d029bea4d48a2517'
	# ConfigTicket['sandbox'] = True
	# ConfigTicket['MessageTitle'] = 'Pagamento Processado'
	# ConfigTicket['MessageContent'] = '''
	# 	Olá {{Data['ClinetName']}}!
	# 	Seu pedido {{Data['ProductName']}} com data de vencimento no dia {{Data['ExpireAt']}} acaba de ser processado.
	# 	Par sua comodidade segue seu boleto em anexo.
	# '''
	# ConfigTicket.save()
	# print(ConfigTicket.pretty())


	# # -> add ticket with gerencianet -<
	# Ticket = GerenciaNet()
	# chk = Ticket.addPayment(
	# 	ProductName = 'ProdutctName',
	# 	ProductValue = 1000,
	# 	ExpireAt = '2017-10-04',
	# 	ClinetName = 'Gorbadoc Oldbuck',
	# 	ClientEmail = 'marciomau@gmail.com',
	# 	CPF = '94271564656',
	# 	BirthDay = '1977-01-15',
	# 	PhoneNumber = '5144916523'
	# )

	# if not (chk):
	# 	print(Ticket.getError())
	# else:
	# 	print('success!')
	# Ticket.ProcessPayment();

	# -> set config nfe <-
	# ConfigNfe = keepdict('config/Nfe.json')
	# # ConfigNfe['NextLot'] = 1
	# # ConfigNfe['NextRps'] = 1
	# ConfigNfe['Cnpj'] = '13419453000107'
	# ConfigNfe['InscricaoMunicipal'] = '123456'
	# ConfigNfe['Rps'] = {}
	# ConfigNfe['Rps']['Serie'] = 'NFE'
	# ConfigNfe['Rps']['Tipo'] = '1'
	# ConfigNfe['Rps']['RegimeEspecialTributacao'] = '6'
	# ConfigNfe['Rps']['OptanteSimplesNacional'] = '1'
	# ConfigNfe['Rps']['IncentivoFiscal'] = '2'
	# ConfigNfe['Rps']['IssRetido'] = '2'
	# ConfigNfe['Rps']['ItemListaServico'] = '17.19'
	# ConfigNfe['Rps']['CodigoMunicipio'] = '3205309'
	# ConfigNfe['Rps']['ExigibilidadeISS'] = '1'
	# ConfigNfe['Rps']['MunicipioIncidencia'] = '3205309'
	# ConfigNfe['Items'] = {}
	# ConfigNfe['Items']['DataEmissao'] = {}
	# ConfigNfe['Items']['DataEmissao']['Type'] = 'date'
	# ConfigNfe['Items']['DataEmissao']['Value'] = '0001-01-01'
	# ConfigNfe['Items']['DataEmissao']['Required'] = True
	# ConfigNfe['Items']['DataEmissao']['ListPath'] = ['InfDeclaracaoPrestacaoServico','Rps','DataEmissao']
	
	# ConfigNfe['Items']['Competencia'] = {}
	# ConfigNfe['Items']['Competencia']['Type'] = 'date'
	# ConfigNfe['Items']['Competencia']['Value'] = '0001-01-01'
	# ConfigNfe['Items']['Competencia']['Required'] = True
	# ConfigNfe['Items']['Competencia']['ListPath'] = ['InfDeclaracaoPrestacaoServico','Competencia']

	# ConfigNfe['Items']['ValorServicos'] = {}
	# ConfigNfe['Items']['ValorServicos']['Type'] = 'float'
	# ConfigNfe['Items']['ValorServicos']['Value'] = '0.00'
	# ConfigNfe['Items']['ValorServicos']['Required'] = True
	# ConfigNfe['Items']['ValorServicos']['ListPath'] = ['InfDeclaracaoPrestacaoServico','Servico','Valores','ValorServicos']

	# ConfigNfe['Items']['ValorDeducoes'] = {}
	# ConfigNfe['Items']['ValorDeducoes']['Type'] = 'float'
	# ConfigNfe['Items']['ValorDeducoes']['Value'] = '0.00'
	# ConfigNfe['Items']['ValorDeducoes']['Required'] = False
	# ConfigNfe['Items']['ValorDeducoes']['ListPath'] = ['InfDeclaracaoPrestacaoServico','Servico','Valores','ValorDeducoes']

	# ConfigNfe['Items']['ValorPis'] = {}
	# ConfigNfe['Items']['ValorPis']['Type'] = 'float'
	# ConfigNfe['Items']['ValorPis']['Value'] = '0.00'
	# ConfigNfe['Items']['ValorPis']['Required'] = False
	# ConfigNfe['Items']['ValorPis']['ListPath'] = ['InfDeclaracaoPrestacaoServico','Servico','Valores','ValorPis']

	# ConfigNfe['Items']['ValorCofins'] = {}
	# ConfigNfe['Items']['ValorCofins']['Type'] = 'float'
	# ConfigNfe['Items']['ValorCofins']['Value'] = '0.00'
	# ConfigNfe['Items']['ValorCofins']['Required'] = False
	# ConfigNfe['Items']['ValorCofins']['ListPath'] = ['InfDeclaracaoPrestacaoServico','Servico','Valores','ValorCofins']

	# ConfigNfe['Items']['ValorInss'] = {}
	# ConfigNfe['Items']['ValorInss']['Type'] = 'float'
	# ConfigNfe['Items']['ValorInss']['Value'] = '0.00'
	# ConfigNfe['Items']['ValorInss']['Required'] = False
	# ConfigNfe['Items']['ValorInss']['ListPath'] = ['InfDeclaracaoPrestacaoServico','Servico','Valores','ValorInss']

	# ConfigNfe['Items']['ValorIr'] = {}
	# ConfigNfe['Items']['ValorIr']['Type'] = 'float'
	# ConfigNfe['Items']['ValorIr']['Value'] = '0.00'
	# ConfigNfe['Items']['ValorIr']['Required'] = False
	# ConfigNfe['Items']['ValorIr']['ListPath'] = ['InfDeclaracaoPrestacaoServico','Servico','Valores','ValorIr']

	# ConfigNfe['Items']['ValorCsll'] = {}
	# ConfigNfe['Items']['ValorCsll']['Type'] = 'float'
	# ConfigNfe['Items']['ValorCsll']['Value'] = '0.00'
	# ConfigNfe['Items']['ValorCsll']['Required'] = False
	# ConfigNfe['Items']['ValorCsll']['ListPath'] = ['InfDeclaracaoPrestacaoServico','Servico','Valores','ValorCsll']

	# ConfigNfe['Items']['OutrasRetencoes'] = {}
	# ConfigNfe['Items']['OutrasRetencoes']['Type'] = 'float'
	# ConfigNfe['Items']['OutrasRetencoes']['Value'] = '0.00'
	# ConfigNfe['Items']['OutrasRetencoes']['Required'] = False
	# ConfigNfe['Items']['OutrasRetencoes']['ListPath'] = ['InfDeclaracaoPrestacaoServico','Servico','Valores','OutrasRetencoes']

	# ConfigNfe['Items']['ValorIss'] = {}
	# ConfigNfe['Items']['ValorIss']['Type'] = 'float'
	# ConfigNfe['Items']['ValorIss']['Value'] = '0.00'
	# ConfigNfe['Items']['ValorIss']['Required'] = False
	# ConfigNfe['Items']['ValorIss']['ListPath'] = ['InfDeclaracaoPrestacaoServico','Servico','Valores','ValorIss']

	# ConfigNfe['Items']['Aliquota'] = {}
	# ConfigNfe['Items']['Aliquota']['Type'] = 'float'
	# ConfigNfe['Items']['Aliquota']['Value'] = '0.00'
	# ConfigNfe['Items']['Aliquota']['Required'] = False
	# ConfigNfe['Items']['Aliquota']['ListPath'] = ['InfDeclaracaoPrestacaoServico','Servico','Valores','Aliquota']

	# ConfigNfe['Items']['DescontoIncondicionado'] = {}
	# ConfigNfe['Items']['DescontoIncondicionado']['Type'] = 'float'
	# ConfigNfe['Items']['DescontoIncondicionado']['Value'] = '0.00'
	# ConfigNfe['Items']['DescontoIncondicionado']['Required'] = False
	# ConfigNfe['Items']['DescontoIncondicionado']['ListPath'] = ['InfDeclaracaoPrestacaoServico','Servico','Valores','DescontoIncondicionado']

	# ConfigNfe['Items']['DescontoCondicionado'] = {}
	# ConfigNfe['Items']['DescontoCondicionado']['Type'] = 'float'
	# ConfigNfe['Items']['DescontoCondicionado']['Value'] = '0.00'
	# ConfigNfe['Items']['DescontoCondicionado']['Required'] = False
	# ConfigNfe['Items']['DescontoCondicionado']['ListPath'] = ['InfDeclaracaoPrestacaoServico','Servico','Valores','DescontoIncondicionado']

	# ConfigNfe['Items']['Discriminacao'] = {}
	# ConfigNfe['Items']['Discriminacao']['Type'] = 'string'
	# ConfigNfe['Items']['Discriminacao']['Value'] = 'xxxxxx'
	# ConfigNfe['Items']['Discriminacao']['Required'] = True
	# ConfigNfe['Items']['Discriminacao']['ListPath'] = ['InfDeclaracaoPrestacaoServico','Servico','Discriminacao']

	# ConfigNfe['Items']['Cnpj'] = {}
	# ConfigNfe['Items']['Cnpj']['Type'] = 'cnpj'
	# ConfigNfe['Items']['Cnpj']['Value'] = '00000000000000'
	# ConfigNfe['Items']['Cnpj']['Required'] = True
	# ConfigNfe['Items']['Cnpj']['ListPath'] = ['InfDeclaracaoPrestacaoServico','Tomador','IdentificacaoTomador','CpfCnpj','Cnpj']

	# ConfigNfe['Items']['Cpf'] = {}
	# ConfigNfe['Items']['Cpf']['Type'] = 'cpf'
	# ConfigNfe['Items']['Cpf']['Value'] = '00000000000'
	# ConfigNfe['Items']['Cpf']['Required'] = False
	# ConfigNfe['Items']['Cpf']['ListPath'] = ['InfDeclaracaoPrestacaoServico','Tomador','IdentificacaoTomador','CpfCnpj','Cnpj']

	# ConfigNfe['Items']['RazaoSocial'] = {}
	# ConfigNfe['Items']['RazaoSocial']['Type'] = 'string'
	# ConfigNfe['Items']['RazaoSocial']['Value'] = 'xxxxxx'
	# ConfigNfe['Items']['RazaoSocial']['Required'] = True
	# ConfigNfe['Items']['RazaoSocial']['ListPath'] = ['InfDeclaracaoPrestacaoServico','Tomador','RazaoSocial']

	# ConfigNfe['Items']['Numero'] = {}
	# ConfigNfe['Items']['Numero']['Type'] = 'string'
	# ConfigNfe['Items']['Numero']['Value'] = 'xxxxxx'
	# ConfigNfe['Items']['Numero']['Required'] = True
	# ConfigNfe['Items']['Numero']['ListPath'] = ['InfDeclaracaoPrestacaoServico','Tomador','Endereco','Numero']

	# ConfigNfe['Items']['Bairro'] = {}
	# ConfigNfe['Items']['Bairro']['Type'] = 'string'
	# ConfigNfe['Items']['Bairro']['Value'] = 'xxxxxx'
	# ConfigNfe['Items']['Bairro']['Required'] = True
	# ConfigNfe['Items']['Bairro']['ListPath'] = ['InfDeclaracaoPrestacaoServico','Tomador','Endereco','Bairro']

	# ConfigNfe['Items']['CodigoMunicipio'] = {}
	# ConfigNfe['Items']['CodigoMunicipio']['Type'] = 'int'
	# ConfigNfe['Items']['CodigoMunicipio']['Value'] = 0
	# ConfigNfe['Items']['CodigoMunicipio']['Required'] = True
	# ConfigNfe['Items']['CodigoMunicipio']['ListPath'] = ['InfDeclaracaoPrestacaoServico','Tomador','Endereco','CodigoMunicipio']

	# ConfigNfe['Items']['Uf'] = {}
	# ConfigNfe['Items']['Uf']['Type'] = 'string'
	# ConfigNfe['Items']['Uf']['Value'] = 'xxxxxx'
	# ConfigNfe['Items']['Uf']['Required'] = True
	# ConfigNfe['Items']['Uf']['ListPath'] = ['InfDeclaracaoPrestacaoServico','Tomador','Endereco','Uf']

	# ConfigNfe.save()
	# NFE = Nfe()
	# RPS = NFE.createLot()
	# RPS = NFE.createRps()
	# -> new purchases
	# B = Billing()
	# B.chargeNewOrders()
	# withSignature = keepdict('nfe/xmls/models/exemploXMLcomAssinatura.xml')
	# withSignature.save()
	# S = Signature()
	# print(S.get())
	pass